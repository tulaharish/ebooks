/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    paths: {
        'bootstrap' : "js/bootstrap",
        'slick' : "js/slick.min",
    },
    shim: {
        'bootstrap':{
            deps: ['jquery']
        },
        'slick':{
            deps: ['jquery']
        }        
    } 
};